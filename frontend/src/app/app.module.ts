import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from "./material.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { RiskListService } from "./services/risk-list.service";
import { RiskDetailService } from "./services/risk-detail.service";
import { PolicyListService } from "./services/policy-list.service";
import { PolicyDetailService } from "./services/policy-detail.service";
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { RisksListComponent } from './risks-list/risks-list.component';
import { RisksDetailComponent } from './risks-detail/risks-detail.component';
import {HttpClientModule} from "@angular/common/http";
import { PolicyListComponent } from './policy-list/policy-list.component';
import { PolicyDetailComponent } from './policy-detail/policy-detail.component';
import {HashLocationStrategy, LocationStrategy} from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    RisksListComponent,
    RisksDetailComponent,
    PolicyListComponent,
    PolicyDetailComponent
  ],
  imports: [
    MaterialModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    RiskListService,
    RiskDetailService,
    PolicyListService,
    PolicyDetailService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
