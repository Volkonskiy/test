import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import  {RisksListComponent} from './risks-list/risks-list.component';
import {RisksDetailComponent} from "./risks-detail/risks-detail.component";
import {PolicyListComponent} from "./policy-list/policy-list.component";
import {PolicyDetailComponent} from "./policy-detail/policy-detail.component";


const routes: Routes = [
  {
    path: '',
    component: PolicyListComponent
  },
  {
    path: 'policy-detail/:id',
    component: PolicyDetailComponent
  },{
    path: 'risks-list',
    component: RisksListComponent
  },
  {
    path: 'risks-detail/:id',
    component: RisksDetailComponent
  },
  {
    path: '**',
    component: PolicyListComponent
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
