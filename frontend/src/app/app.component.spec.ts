import { TestBed, ComponentFixture, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router'
import { By } from '@angular/platform-browser';
import { Location, CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { MaterialModule } from "./material.module";

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MaterialModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();

  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));

  it(`should have span with title 'Risk Types Test'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('span').textContent).toEqual('Risk Types Test');
  }));

  it(`should contain links with text 'Policies List' and 'Risk Types List'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    let aTags = fixture.debugElement.queryAll(By.css('a'));
    expect(aTags[0].nativeElement.textContent).toEqual('Policies List');
    expect(aTags[1].nativeElement.textContent).toEqual('Risk Types List');
  }));

  it('should route to policy list', async(inject([Router, Location], (router: Router, location: Location) => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    let aTags = fixture.debugElement.queryAll(By.css('a'));
    aTags[0].nativeElement.click();
    fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/');
    });
  })));
});
