import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PolicyDetailService {

  private baseUrl = window.location.protocol + '//' + window.location.host + '/api/policy-field/';

  fieldsList = [];
  private _fields = new BehaviorSubject<any>(this.fieldsList);
  fields = this._fields.asObservable();

  constructor(private http: HttpClient) { }

  updateFieldsList(id): void{
    this.http.get(this.baseUrl+'list/'+id).subscribe(res => this._fields.next(res));
  }

  updateField(fieldId, value) {
    this.http.put(this.baseUrl + 'update/' + fieldId, {new_field_value: value}).subscribe(res => {
      }
    );
  }
}
