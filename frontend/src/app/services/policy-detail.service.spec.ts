import { TestBed, inject } from '@angular/core/testing';
import {HttpClientModule} from "@angular/common/http";

import { PolicyDetailService } from './policy-detail.service';

describe('PolicyDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [PolicyDetailService]
    });
  });

  it('should be created', inject([PolicyDetailService], (service: PolicyDetailService) => {
    expect(service).toBeTruthy();
  }));
});
