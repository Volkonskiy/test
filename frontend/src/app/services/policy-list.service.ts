import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";

@Injectable({
  providedIn: 'root'
})
export class PolicyListService {
  private baseUrl = window.location.protocol + '//' + window.location.host + '/api/policy/';
  private riskListUrl = window.location.protocol + '//' + window.location.host + '/api/risk-type/list';

  policyList = [];
  private _policies = new BehaviorSubject<any>(this.policyList);
  policies = this._policies.asObservable();

  riskList = [];
  private _risks = new BehaviorSubject<any>(this.riskList);
  risks = this._risks.asObservable();

  constructor(private http: HttpClient) { }

  updatePolicyList(): void {
        this.http.get(this.baseUrl+'list').subscribe(res => this._policies.next(res));
    }

  deletePolicy(i){
    this.http.delete(this.baseUrl+'delete/'+i).subscribe(res => {
      this.updatePolicyList();
      }
    );
  }

  addPolicy(type){
    this.http.post(this.baseUrl+'create', {risk_type: type}).subscribe(res => {
      this.updatePolicyList();
      }
    );
  }

  getRiskList(): Observable <any> {
    return this.http.get(this.riskListUrl);
  }
}
