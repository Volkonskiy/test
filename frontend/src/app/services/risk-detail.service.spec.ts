import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RiskDetailService } from './risk-detail.service';

describe('RiskDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RiskDetailService]
    });
  });

  // it('should be initialized', inject([RiskDetailService], (authService: RiskDetailService) => {
  //   expect(authService).toBeTruthy();
  // }));
});
