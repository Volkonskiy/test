import { TestBed, inject } from '@angular/core/testing';

import { RiskListService } from './risk-list.service';
import {HttpClientModule} from "@angular/common/http";

describe('RiskListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [RiskListService]
    });
  });

  // it('should be created', inject([RiskListService], (service: RiskListService) => {
  //   expect(service).toBeTruthy();
  // }));
});
