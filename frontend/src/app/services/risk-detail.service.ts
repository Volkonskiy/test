import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class RiskDetailService {

  private baseUrl = window.location.protocol + '//' + window.location.host + '/api/risk-field/';

  fieldsList = [];
  private _fields = new BehaviorSubject<any>(this.fieldsList);
  fields = this._fields.asObservable();

  constructor(private http: HttpClient) { }

  updateFieldsList(id): void{
    this.http.get(this.baseUrl+'list/'+id).subscribe(res => this._fields.next(res));
  }

  addField(riskId, name, type, value, enumChecked, enumText) {
      this.http.post(this.baseUrl+'create/'+riskId, {
        name: name,
        field_type: type,
        field_value: value,
        enum: enumChecked,
        enum_values: enumText
        }).subscribe(res => {
          this.updateFieldsList(riskId);
      }
    );
  }

  deleteField(fieldId, riskId) {
    this.http.delete(this.baseUrl + 'delete/' + fieldId).subscribe(res => {
        this.updateFieldsList(riskId);
      }
    );
  }

  updateField(fieldId, riskId, value) {
    this.http.put(this.baseUrl + 'update/' + fieldId, {field_value: value}).subscribe(res => {
        // this.updateFieldsList(riskId);
      }
    );
  }
}
