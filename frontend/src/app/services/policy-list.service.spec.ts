import { TestBed, inject } from '@angular/core/testing';

import { PolicyListService } from './policy-list.service';
import {HttpClientModule} from "@angular/common/http";

describe('PolicyListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [PolicyListService]
    });
  });

  it('should be created', inject([PolicyListService], (service: PolicyListService) => {
    expect(service).toBeTruthy();
  }));
});
