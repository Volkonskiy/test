import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class RiskListService {
  private baseUrl = window.location.protocol + '//' + window.location.host + '/api/risk-type/';

  risksList = [];
  private _risks = new BehaviorSubject<any>(this.risksList);
  risks = this._risks.asObservable();

  constructor(private http: HttpClient) { }

  updateRiskTypeList(): void{
        this.http.get(this.baseUrl+'list').subscribe(res => this._risks.next(res));
    }

  deleteRisk(i){
    this.http.delete(this.baseUrl+'delete/'+i).subscribe(res => {
      this.updateRiskTypeList();
      }
    );
  }

  addRisk(name){
    this.http.post(this.baseUrl+'create', {name: name}).subscribe(res => {
      this.updateRiskTypeList();
      }
    );
  }
}
