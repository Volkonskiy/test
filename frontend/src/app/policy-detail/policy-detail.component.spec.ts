import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from "../material.module";
import { PolicyDetailComponent } from './policy-detail.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";

describe('PolicyDetailComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule,
        HttpClientModule
      ],
      declarations: [ PolicyDetailComponent ]
    });
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(PolicyDetailComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });
});
