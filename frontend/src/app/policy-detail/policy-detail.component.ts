import { Component, OnInit } from '@angular/core';
import {PolicyDetailService} from "../services/policy-detail.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-policy-detail',
  templateUrl: './policy-detail.component.html',
  styleUrls: ['./policy-detail.component.scss']
})
export class PolicyDetailComponent implements OnInit {
  fields = [];
  policyId: number;

  constructor(private route: ActivatedRoute, private router: Router, private _data: PolicyDetailService) {
    this.route.params.subscribe(res=> this.policyId = res.id);
  }

   ngOnInit() {
    this._data.fields.subscribe(res => this.fields = res);
    this._data.updateFieldsList(this.policyId);
  }

  updateField(fieldId, type, event) {
    let value = '';
    switch (type) {
      case 3:
        value = (event.checked) ? "1" : "0";
        break;
      case 4:
        value = event.target.value.toLocaleDateString();
        break;
      case 5:
        value = event.value.toString();
        break;
      default:
        value = event.target.value.toString();
    }
    this._data.updateField(fieldId, value);
  }

}
