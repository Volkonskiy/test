import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RisksListComponent } from './risks-list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MaterialModule} from "../material.module";
import {HttpClientModule} from "@angular/common/http";

describe('RisksListComponent', () => {
  let component: RisksListComponent;
  let fixture: ComponentFixture<RisksListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [ RisksListComponent ]
    });
    fixture = TestBed.createComponent(RisksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
