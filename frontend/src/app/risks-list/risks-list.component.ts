import { Component, OnInit } from '@angular/core';
import {RiskListService} from "../services/risk-list.service";

@Component({
  selector: 'app-risks-list',
  templateUrl: './risks-list.component.html',
  styleUrls: ['./risks-list.component.scss']
})
export class RisksListComponent implements OnInit {

  btnText: string = "Add Risk Type";
  riskName: string = "";
  risks = [];

  constructor(private _data: RiskListService) { }

  ngOnInit() {
    this._data.risks.subscribe(res => this.risks = res);
    this._data.updateRiskTypeList();
  }

  addRisk(){
    this._data.addRisk(this.riskName);
    this.riskName = "";
  }

  deleteRisk(i){
    this._data.deleteRisk(i);
  };

}
