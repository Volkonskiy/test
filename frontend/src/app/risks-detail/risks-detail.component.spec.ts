// import { RisksDetailComponent } from './risks-detail.component';
// import { RiskDetailService } from "../services/risk-detail.service";
// import {HttpClient} from "@angular/common/http";
//
//
// describe('RisksDetailComponent', () => {
//   let httpClient: HttpClient;
//   let component: RisksDetailComponent;
//   let service: RiskDetailService;
//
//   beforeEach(() => {
//     httpClient = new HttpClient();
//     service = new RiskDetailService();
//     component = new RisksDetailComponent();
//   });
// }


import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from "../material.module";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RisksDetailComponent } from './risks-detail.component';

describe('RisksDetailComponent', () => {
  let component: RisksDetailComponent;
  let fixture: ComponentFixture<RisksDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [ RisksDetailComponent ]
    }).compileComponents();
  }));

  // it('should create', () => {
  //   fixture = TestBed.createComponent(RisksDetailComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  //   expect(component).toBeTruthy();
  // });
});
