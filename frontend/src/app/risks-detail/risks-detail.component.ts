import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Router} from "@angular/router";
import {RiskDetailService} from "../services/risk-detail.service";

@Component({
  selector: 'app-risks-detail',
  templateUrl: './risks-detail.component.html',
  styleUrls: ['./risks-detail.component.scss']
})
export class RisksDetailComponent implements OnInit {

  fields = [];
  riskId: number;

  addFieldName: string = '';
  addFieldType: number = 1;
  addFieldValue: any = 0;
  addFieldTypeStr: string = "number";

  enumText;
  enumChecked: boolean = false;
  enumList = [];

  constructor(private route: ActivatedRoute, private router: Router, private _data: RiskDetailService) {
    this.route.params.subscribe(res=> this.riskId = res.id);
  }

   ngOnInit() {
    this._data.fields.subscribe(res => this.fields = res);
    this._data.updateFieldsList(this.riskId);
  }

  addField() {
    let value = '';
    let fieldType: number;
    let enumText: string = "";
    switch (this.addFieldTypeStr) {
      case "number":
        fieldType=1;
        break;
      case "text":
        fieldType=2;
        break;
      case "checkbox":
        fieldType=3;
        break;
      case "date":
        fieldType=4;
        break;
    }

    if (this.enumChecked) {
      enumText = this.enumList.map(String).join('\n')
    }

    if (this.addFieldTypeStr=="number") {
      fieldType=1;
    }
    else if (this.addFieldTypeStr=="text") {
      fieldType=2;
    }
    else if (this.addFieldTypeStr=="checkbox") {
      fieldType=3;
    }
    else if (this.addFieldTypeStr=="date") {value = this.addFieldValue; fieldType=4}

    if (this.enumChecked) {
      enumText = this.enumList.map(String).join('\n')
    }
    this._data.addField(this.riskId, this.addFieldName, fieldType, value, this.enumChecked, enumText)
  }

  deleteField(fieldId) {
    this._data.deleteField(fieldId, this.riskId);
  }

  enumChange() {
    this.enumList = [];
    if (this.addFieldTypeStr == "checkbox" && this.enumChecked) {
      this.addFieldTypeStr = "number";
    }
  }

  enumAdd() {
    let value;
    if (this.addFieldTypeStr == "date"){
      value = this.enumText.toLocaleDateString();
    } else {
      value = this.enumText.toString();
    }

    this.enumList.push(value)
  }

}
