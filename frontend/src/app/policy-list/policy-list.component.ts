import { Component, OnInit } from '@angular/core';
import {PolicyListService} from "../services/policy-list.service";
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-policy-list',
  templateUrl: './policy-list.component.html',
  styleUrls: ['./policy-list.component.scss']
})
export class PolicyListComponent implements OnInit {
  riskControl = new FormControl('', [Validators.required]);

  btnText: string = "Add Policy";
  policyName: string = "";
  policies = [];
  risksList;

  constructor(private _data: PolicyListService) { }

  ngOnInit() {
    this._data.policies.subscribe(res => this.policies = res);
    this._data.updatePolicyList();
    this._data.getRiskList().subscribe(res => {
      this.risksList = res;
    });
  }

  addPolicy(){
    this._data.addPolicy(this.riskControl.value);
  }

  deletePolicy(i){
    this._data.deletePolicy(i);
  };


}
