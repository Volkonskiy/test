import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from "../material.module";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";

import { PolicyListComponent } from './policy-list.component';
import {RouterTestingModule} from "@angular/router/testing";

describe('PolicyListComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule
      ],
      declarations: [ PolicyListComponent ]
    });
  });

  it('form', () => {
    const fixture = TestBed.createComponent(PolicyListComponent);
    const component = fixture.debugElement.componentInstance;
    component.ngOnInit();
    console.log(component.form);
  });

  it('should create', () => {
    const fixture = TestBed.createComponent(PolicyListComponent);
    const component = fixture.componentInstance;
    component.ngOnInit();
    expect(component).toBeTruthy();
  });
});
