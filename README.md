# Risks Type Test Project

## Technical stack:

### Frontend:

- JavaScript ES6
- AngularJS 5)

### Backend:

- Python 3.6
- Django 2.0.*
- Django Rest Framework 3.8.*

## Setup:

### Requirements:

- virtualenv
- Python 3.6

### Initialize project:

#### 1. Clone the project:

* Run following command for clone the project from git
    ```bash
    git clone https://gitlab.com/Volkonskiy/test.git
    ```

#### 2. Backend:

* Create python environment:

    ```bash
    virtualenv -p python3 envname
    pip install -r requirements.txt
    ```

#### 3. Open the project with **PyCharm**


#### 4. Migrate database:

* It is test project, so I use sqlite here)



As a testing mode It can be ran locally:

python manage.py runserver 127.0.0.1:8000
and
cd frontend
npm start
