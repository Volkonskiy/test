from rest_framework import serializers
from datetime import datetime
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
    RetrieveAPIView,
)
from risks.apps.risk_types.api.serializers import (
    RiskTypeSerializer,
    RiskFieldSerializer,
    PolicySerializer,
    PolicyFieldSerializer,
)
from risks.apps.risk_types.models import RiskType, RiskField, Policy, PolicyField, IntField, CharField, BoolField


class RiskTypeListAPIView(ListAPIView):
    queryset = RiskType.objects.all()
    serializer_class = RiskTypeSerializer


class RiskTypeDetailAPIView(RetrieveAPIView):
    queryset = RiskType.objects.all()
    serializer_class = RiskTypeSerializer


class RiskTypeCreateAPIView(CreateAPIView):
    serializer_class = RiskTypeSerializer


class RiskTypeUpdateAPIView(UpdateAPIView):
    queryset = RiskType.objects.all()
    serializer_class = RiskTypeSerializer


class RiskTypeDeleteAPIView(DestroyAPIView):
    queryset = RiskType.objects.all()


class RiskFieldListAPIView(ListAPIView):
    serializer_class = RiskFieldSerializer

    def get_queryset(self):
        return RiskField.objects.filter(risk_type=self.kwargs['pk'])


class RiskFieldDetailAPIView(RetrieveAPIView):
    queryset = RiskField.objects.all()
    serializer_class = RiskFieldSerializer


class RiskFieldCreateAPIView(CreateAPIView):
    serializer_class = RiskFieldSerializer

    def perform_create(self, serializer):
        if not RiskType.objects.filter(id=self.kwargs['pk']).exists():
            raise serializers.ValidationError()
        serializer.validated_data["risk_type_id"] = self.kwargs['pk']
        serializer.save()


class RiskFieldUpdateAPIView(UpdateAPIView):
    queryset = RiskField.objects.all()
    serializer_class = RiskFieldSerializer


class RiskFieldDeleteAPIView(DestroyAPIView):
    queryset = RiskField.objects.all()


class PolicyListAPIView(ListAPIView):
    queryset = Policy.objects.all()
    serializer_class = PolicySerializer


class PolicyDetailAPIView(RetrieveAPIView):
    queryset = Policy.objects.all()
    serializer_class = PolicySerializer


class PolicyCreateAPIView(CreateAPIView):
    serializer_class = PolicySerializer


class PolicyUpdateAPIView(UpdateAPIView):
    queryset = Policy.objects.all()
    serializer_class = PolicySerializer


class PolicyDeleteAPIView(DestroyAPIView):
    queryset = Policy.objects.all()


class PolicyFieldListAPIView(ListAPIView):
    serializer_class = PolicyFieldSerializer

    def get_queryset(self):
        return PolicyField.objects.filter(policy=self.kwargs['pk']).select_related()


class PolicyFieldDetailAPIView(RetrieveAPIView):
    queryset = PolicyField.objects.all()
    serializer_class = PolicyFieldSerializer


class PolicyFieldCreateAPIView(CreateAPIView):
    serializer_class = PolicyFieldSerializer


class PolicyFieldUpdateAPIView(UpdateAPIView):
    queryset = PolicyField.objects.all()
    serializer_class = PolicyFieldSerializer

    def perform_update(self, serializer):
        new_value = serializer.validated_data["new_field_value"]
        try:
            policy_field = PolicyField.objects.select_related().get(id=self.kwargs['pk'])
        except PolicyField.DoesNotExist:
            raise serializers.ValidationError
        field_type = policy_field.field_type.field_type
        value_data_model = policy_field.field_value

        if field_type in (1, 2):
            new_value = type(value_data_model.value)(new_value)
        elif field_type == 3:
            new_value = type(value_data_model.value)(int(new_value))
        elif field_type == 4:
            new_value = datetime.strptime(new_value, "%m/%d/%Y").date()
        else:
            raise serializers.ValidationError()
        value_data_model.value = new_value
        value_data_model.save()


class PolicyFieldDeleteAPIView(DestroyAPIView):
    queryset = PolicyField.objects.all()
