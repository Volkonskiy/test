from rest_framework.serializers import ModelSerializer, SerializerMethodField, CharField
from risks.apps.risk_types.models import RiskType, RiskField, Policy, PolicyField, DateField


class RiskTypeSerializer(ModelSerializer):
    class Meta:
        model = RiskType
        fields = [
            "id",
            "name",
        ]


class RiskFieldSerializer(ModelSerializer):
    class Meta:
        model = RiskField
        fields = [
            "id",
            "name",
            "field_type",
            "enum",
            "enum_values",

        ]


class PolicySerializer(ModelSerializer):
    class Meta:
        model = Policy
        fields = [
            "id",
            "serial_number",
            "created_date",
            "risk_type",
        ]
        read_only_fields = ('id', 'serial_number', "created_date")


class PolicyFieldSerializer(ModelSerializer):
    field_type = RiskFieldSerializer(read_only=True)
    new_field_value = CharField(max_length=256, required=True, write_only=True)
    field_value = SerializerMethodField()

    class Meta:
        model = PolicyField
        fields = [
            'id',
            'field_type',
            'field_value',
            'new_field_value',
        ]
        read_only_fields = ('id', 'field_type', "field_value")

    def get_field_value(self, field):
        if isinstance(field.field_value, DateField) and field.field_type.enum:
            return f'{field.field_value.value.month}/{field.field_value.value.day}/{field.field_value.value.year}'
        return field.field_value.value
