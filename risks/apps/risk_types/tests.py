import json
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status

from risks.apps.risk_types.models import RiskType, RiskField, Policy, PolicyField

from risks.apps.risk_types.api.serializers import RiskFieldSerializer, PolicyFieldSerializer


class RiskTests(TestCase):
    """
    Well basically, there is nothing to test here because only build in rest_framework methods were used:)
    """

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        self.client = APIClient()

    def test_risk_list(self):
        pass

    def test_risk_detail(self):
        pass

    def test_risk_create(self):
        pass

    def test_risk_update(self):
        pass

    def test_risk_delete(self):
        pass


class RiskFieldsTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        self.client = APIClient()

    def test_risk_fields_detail(self):
        pass

    def test_risk_fields_create(self):
        pass

    def test_risk_fields_update(self):
        pass

    def test_risk_delete(self):
        pass


class RiskFieldsListTests(TestCase):
    fixtures = ['initial_data']

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        self.client = APIClient()

    def test_risk_fields_list(self):
        risk_type = RiskType.objects.first()
        risk_fields = RiskField.objects.filter(risk_type=risk_type)
        serializer = RiskFieldSerializer(risk_fields, many=True)
        response = self.client.get(reverse('risk_type:risk_field_list', kwargs={'pk': risk_type.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)


class RiskFieldsCreateTests(TestCase):
    fixtures = ['initial_data']

    @classmethod
    def setUpTestData(cls):
        cls.valid_field_data = {'name': 'Test', 'field_type': 1}
        cls.invalid_field_data = {'name': '', 'field_type': 1}
        cls.wrong_field_type_data = {'name': 'Test', 'field_type': 0}

    def setUp(self):
        self.client = APIClient()

    def test_risk_fields_create_valid_data(self):
        risk_type = RiskType.objects.first()
        response = self.client.post(
            reverse('risk_type:risk_field_create', kwargs={'pk': risk_type.pk}),
            data=json.dumps(self.valid_field_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_risk_fields_create_invalid_data(self):
        risk_type = RiskType.objects.first()
        response = self.client.post(
            reverse('risk_type:risk_field_create', kwargs={'pk': risk_type.pk}),
            data=json.dumps(self.invalid_field_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_risk_fields_create_wrong_field_type(self):
        risk_type = RiskType.objects.first()
        response = self.client.post(
            reverse('risk_type:risk_field_create', kwargs={'pk': risk_type.pk}),
            data=json.dumps(self.wrong_field_type_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_risk_fields_create_wrong_risk_type_id(self):
        risk_type_pk = RiskType.objects.last().pk + 1
        response = self.client.post(
            reverse('risk_type:risk_field_create', kwargs={'pk': risk_type_pk}),
            data=json.dumps(self.valid_field_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PolicyFieldsListTests(TestCase):
    fixtures = ['initial_data']

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        self.client = APIClient()

    def test_policy_fields_list(self):
        policy = Policy.objects.first()
        policy_fields = PolicyField.objects.filter(policy=policy)
        serializer = PolicyFieldSerializer(policy_fields, many=True)
        response = self.client.get(reverse('risk_type:policy_field_list', kwargs={'pk': policy.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_policy_fields_list_wrong_policy_id(self):
        policy_pk = Policy.objects.last().pk + 1
        response = self.client.get(reverse('risk_type:policy_field_list', kwargs={'pk': policy_pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [])


class PolicyFieldsUpdateTests(TestCase):
    fixtures = ['initial_data']

    @classmethod
    def setUpTestData(cls):
        cls.valid_int_field_data = {'new_field_value': 256}
        cls.invalid_char_field_data = {'new_field_value': ''}
        cls.wrong_field_data = {'new_field_value': [1, 2, 3]}

    def setUp(self):
        self.client = APIClient()
        self.risk_type = RiskType.objects.create(name='Test')

        self.risk_int_field = RiskField.objects.create(
            name='int_test',
            field_type=RiskField.FIELD_TYPE__NUMBER,
            risk_type=self.risk_type,
        )
        self.risk_char_field = RiskField.objects.create(
            name='char_test',
            field_type=RiskField.FIELD_TYPE__CHAR,
            risk_type=self.risk_type,
        )

        self.policy = Policy.objects.create(risk_type=self.risk_type)
        self.policy_int_field = PolicyField.objects.get(policy=self.policy, field_type=self.risk_int_field)
        self.policy_char_field = PolicyField.objects.get(policy=self.policy, field_type=self.risk_int_field)

    def test_risk_fields_update_valid_data(self):

        response = self.client.put(
            reverse('risk_type:policy_field_update', kwargs={'pk': self.policy_int_field.pk}),
            data=json.dumps(self.valid_int_field_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_risk_fields_update_invalid_data(self):
        response = self.client.put(
            reverse('risk_type:policy_field_update', kwargs={'pk': self.policy_char_field.pk}),
            data=json.dumps(self.invalid_char_field_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_risk_fields_update_wrong_policy_field_id(self):
        wrong_policy_field_pk = PolicyField.objects.last().id + 1
        response = self.client.put(
            reverse('risk_type:policy_field_update', kwargs={'pk': wrong_policy_field_pk}),
            data=json.dumps(self.valid_int_field_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_risk_fields_update_wrong_field_data(self):
        response = self.client.put(
            reverse('risk_type:policy_field_update', kwargs={'pk': self.policy_int_field.pk}),
            data=json.dumps(self.wrong_field_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
