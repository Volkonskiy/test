import sys
from datetime import date

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class PolicyManager(models.Manager):

    def create(self, **kwargs):
        obj = super().create(**kwargs)
        # random serial number generator)
        obj.serial_number = f"{obj.risk_type.name[:3].upper()}-{obj.id}{date.today().strftime('%d%m%y')}"
        obj.save()
        obj.create_policy_fields()
        return obj


class RiskType(models.Model):
    """
    This is the base model that will handle User created custom risk types
    """
    name = models.CharField(max_length=256, unique=True)


class RiskField(models.Model):
    """
    Here we create fields that can be added to any of our risk type
    """
    FIELD_TYPE__NUMBER = 1
    FIELD_TYPE__CHAR = 2
    FIELD_TYPE__BOOLEAN = 3
    FIELD_TYPE__DATE = 4

    # This are names of models to store the data
    ITEM_MP_STATUS_CHOICES = (
        (FIELD_TYPE__NUMBER, "IntField"),
        (FIELD_TYPE__CHAR, "CharField"),
        (FIELD_TYPE__BOOLEAN, "BoolField"),
        (FIELD_TYPE__DATE, "DateField"),
    )

    name = models.CharField(max_length=256, unique=True)
    field_type = models.SmallIntegerField(choices=ITEM_MP_STATUS_CHOICES)
    risk_type = models.ForeignKey(RiskType, on_delete=models.CASCADE)

    enum = models.BooleanField(default=False)
    # \n separated values than used to make enum list (drop-down)
    enum_values = models.TextField(blank=True, null=True)


class Policy(models.Model):
    objects = PolicyManager()

    serial_number = models.CharField(max_length=32)
    created_date = models.DateField(auto_now_add=True)
    risk_type = models.ForeignKey(RiskType, on_delete=models.CASCADE)

    @staticmethod
    def str_to_class(name):
        return getattr(sys.modules[__name__], name)

    def create_policy_fields(self):
        """
        When Policy created, policy fields should be automaticaly created
        """
        for field in RiskField.objects.filter(risk_type_id=self.risk_type):
            data_model = self.str_to_class(field.get_field_type_display())
            value = data_model.objects.create()
            PolicyField.objects.create(
                policy=self,
                field_type=field,
                field_value=value,
            )


class PolicyField(models.Model):
    policy = models.ForeignKey(Policy, on_delete=models.CASCADE)
    field_type = models.ForeignKey(RiskField, on_delete=models.CASCADE)

    field_object = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    field_id = models.PositiveIntegerField()
    field_value = GenericForeignKey('field_object', 'field_id')


class BaseField(models.Model):
    class Meta:
        abstract = True


class IntField(BaseField):
    value = models.IntegerField(blank=True, null=True, default=0)


class CharField(BaseField):
    value = models.CharField(max_length=256, blank=True, null=True, default='')


class BoolField(BaseField):
    value = models.BooleanField(blank=True, default=False)


class DateField(BaseField):
    value = models.DateField(blank=True, null=True, auto_now_add=True)
